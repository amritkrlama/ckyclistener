package com.capiot.ckyclistener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.capiot.ckyclistener.service.FileUploadListener;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootApplication(scanBasePackages= {"com.capiot.ckyclistener"})
public class CkycListenerApplication implements CommandLineRunner{


	@Autowired
	FileUploadListener fileUploadListener;
	
	
	public static void main(String[] args) {
		SpringApplication.run(CkycListenerApplication.class, args);
	}
	
	
	@Bean
	RestTemplate restTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		converter.setObjectMapper(new ObjectMapper());
		restTemplate.getMessageConverters().add(converter);
		return restTemplate;
	}

	@Override
	public void run(String... args) throws Exception {
		fileUploadListener.listenToFileUpload();
	}
}
