package com.capiot.ckyclistener.exception;

public class ResponseFileException extends Exception{

	private static final long serialVersionUID = 1L;
	
	public ResponseFileException(String message) {
		super(message);
	}
	
	public ResponseFileException(String message, Throwable cause) {
		super(message, cause);
	}

}
