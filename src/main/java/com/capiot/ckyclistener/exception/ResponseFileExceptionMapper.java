package com.capiot.ckyclistener.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;


public class ResponseFileExceptionMapper implements ExceptionMapper<ResponseFileException>{
	
	@Override
	public Response toResponse(ResponseFileException exception) {
		return Response.status(Status.FORBIDDEN).entity(exception.getMessage()).build();
	}

}
