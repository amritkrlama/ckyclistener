package com.capiot.ckyclistener.service;

import java.io.File;

import org.springframework.stereotype.Service;

import com.capiot.ckyclistener.exception.ResponseFileException;

@Service
public interface CreateResponseFile {
	
	public File createResponseFile(String FICode, String batchNumber) throws ResponseFileException;
	
}
