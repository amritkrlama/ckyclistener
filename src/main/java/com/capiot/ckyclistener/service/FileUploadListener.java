package com.capiot.ckyclistener.service;

import org.springframework.stereotype.Service;

@Service
public interface FileUploadListener {
	
	public void listenToFileUpload();
}
