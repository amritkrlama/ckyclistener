package com.capiot.ckyclistener.service;

import java.io.IOException;

import org.springframework.stereotype.Service;

@Service
public interface RequestFileReader {
	
	public String getAllData(String fileName) throws IOException;
	public String getBatchNumber(String allData);
	public String getFIReferenceNumber(String allData);
	public String getFICode(String allData);
	public int getNumberOfDetailRecords(String allData);
	
	public void unzipInputFolder(String fileName);
}
