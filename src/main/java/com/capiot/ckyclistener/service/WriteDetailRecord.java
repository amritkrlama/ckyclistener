package com.capiot.ckyclistener.service;

import org.springframework.stereotype.Service;

@Service
public interface WriteDetailRecord {
	
	public String writeRecordType();
	public String writeLineNumber(String singleDetailRecord);
	public String writeBatchName(String batchName);
	public String writeCKYCTempReferenceNumber();
	public String writeRequestType();
	public String writeResponseType();
	public String writeRecordStatus();
	public String writeFIReferenceNumber(String singleDetailRecord);
	public String writeCKYCNumber();
	public String writeRemarks();
	public String writeNewLine();
}
