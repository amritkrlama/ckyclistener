package com.capiot.ckyclistener.service;

import org.springframework.stereotype.Service;

@Service
public interface WriteHeader {
	
	public String writeRecordType();
	public String writeFICode(String FICode);
	public String writeTotalNumberOfRecords(int totalNumberOfRecords);
	public String writeVersionNumber();
	public String writeCreateDate();
	public String writeFillers();
	public String writeNewLine();
	
}
