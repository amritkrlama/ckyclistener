package com.capiot.ckyclistener.service;

import java.io.File;

import org.springframework.stereotype.Service;

@Service
public interface WriteResponseFile {
	
	public void writeResponseFile(String allData, String FICode, String batchNumber, String FIReferenceNumber,  int numberOfDetailRecords, File file);
	public String writeHeader(String allData, String FICode, String batchNumber, String FIReferenceNumber,  int numberOfDetailRecords);
	public String writeDetailRecord(String allData, String FICode, String batchNumber, String FIReferenceNumber,  int numberOfDetailRecords);
}
