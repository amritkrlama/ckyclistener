package com.capiot.ckyclistener.service.impl;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.capiot.ckyclistener.exception.ResponseFileException;
import com.capiot.ckyclistener.service.CreateResponseFile;


@Component
@Configuration
@PropertySource("classpath:ckyc.properties")
public class CreateResponseFileImpl implements CreateResponseFile {
	
	@Autowired
	Environment environment;
	
	private static final Logger logger = LoggerFactory.getLogger(CreateResponseFileImpl.class);
	private static final String INDIAN_TIMEZONE = "GMT+5:30";
	private String home = System.getProperty("user.home");
	public static String responseFileName = null;
	public static String responseFilePath = null;
	
	int counter=1;


	
	/*Method to generate PSF file*/
	@Override
	public File createResponseFile(String FICode, String batchNumber) throws ResponseFileException {	
		String commonPathResponse = home+environment.getProperty("commonPathResponse");
		
		File responseFolder = new File(commonPathResponse);		
		if(!responseFolder.exists()) {
			if(responseFolder.mkdirs()) {
				logger.info("Response Folder has been successfully created at: {}", commonPathResponse);
			}else {
				logger.info("Error in creating the Response Folder {} at: {} ", commonPathResponse);
			}
		}else {
			logger.info("Response Folder already exists at: {}", commonPathResponse);
		}
		
		
		responseFileName = FICode+"_"+getCurrentDateTime()+"_"+batchNumber;	
		responseFilePath = commonPathResponse+responseFileName+".txt";
		File file = null;
		try {
			file = new File(responseFilePath);
			if(file.createNewFile()) {
				logger.info("Response file has been created successfully");
			}else {
				logger.info("Response file already exists.");
			}
		}catch(NullPointerException | IOException exception) {
			throw new ResponseFileException("Not able to create the Response file", exception);
		}
		
		return file;
	}
	
	
	
	
	/*Method to return current date time */
	public String getCurrentDateTime() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
		dateFormat.setTimeZone(TimeZone.getTimeZone(INDIAN_TIMEZONE));
		Date now = new Date();
		String currentDateTime = dateFormat.format(now);
		return currentDateTime;
	}




}
