package com.capiot.ckyclistener.service.impl;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.capiot.ckyclistener.service.CreateResponseFile;
import com.capiot.ckyclistener.service.FileUploadListener;
import com.capiot.ckyclistener.service.RequestFileReader;
import com.capiot.ckyclistener.service.WriteResponseFile;


@Component
@Configuration
@PropertySource("classpath:ckyc.properties")
public class FileUploadListenerImpl implements FileUploadListener{

	@Autowired
	Environment environment;
	
	@Autowired
	CreateResponseFile responseFileWriterService;
	@Autowired
	RequestFileReader requestFileReaderService;
	@Autowired
	WriteResponseFile writeResponseFileService;
	
	
	private static final Logger logger = LoggerFactory.getLogger(FileUploadListener.class);
	private static final String home = System.getProperty("user.home");
	
	
	private static String allData = null;
	private static String batchNumber = null;
	private static String FIReferenceNumber = null;
	private static String FICode = null;
	private static int numberOfDetailRecords = 0;

	public File file = null;
	
	
	/*Listens for any file upload*/
	public void listenToFileUpload() {
	
		final Path commonPathInput = Paths.get(home+environment.getProperty("commonPathInput"));
		
		try {
			
			logger.info("Started listening to Input folder: {}", String.valueOf(commonPathInput));
			WatchService watcher = commonPathInput.getFileSystem().newWatchService();
			commonPathInput.register(watcher, StandardWatchEventKinds.ENTRY_CREATE);
			WatchKey watchKey = null;
			
			
			/*Infinite loop to listen*/
			while(true) {
				watchKey = watcher.take();
				List<WatchEvent<?>> events = watchKey.pollEvents();
				if(events!=null) {
					for(WatchEvent<?> event: events) {
						String fileNameWithoutExtension = getFileNameWithoutExtension(event.context().toString());
						if(getFileExtension(event.context().toString()).equalsIgnoreCase("trg")) {
							
							logger.info("Unzipping the Input folder "+fileNameWithoutExtension+".zip");
							requestFileReaderService.unzipInputFolder(fileNameWithoutExtension);
							
							
							allData = requestFileReaderService.getAllData(fileNameWithoutExtension);
							batchNumber = requestFileReaderService.getBatchNumber(allData);
							FIReferenceNumber = requestFileReaderService.getFIReferenceNumber(allData);
							FICode = requestFileReaderService.getFICode(allData);
							numberOfDetailRecords = requestFileReaderService.getNumberOfDetailRecords(allData);
														
																				
							file=responseFileWriterService.createResponseFile(FICode, batchNumber);
							writeResponseFileService.writeResponseFile(allData, FICode, batchNumber, FIReferenceNumber, 
																					numberOfDetailRecords, file);
						}
					}
				}		
				if(!watchKey.reset()) {
					logger.info("The directory is inaccessible.");
					break;
				}
			}
		}catch(Exception exception) {
			logger.debug("Exception occured in Listener");
			exception.printStackTrace();
		}
	}
	
	
	
	
	/*Method to get the file extension*/
	 private static String getFileExtension(String fileName) {
	        if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
	        return fileName.substring(fileName.lastIndexOf(".")+1);
	        else return "";
	    }
	 
	 
	 
	 
	 /*Method to get the file name without extension*/
	 private static String getFileNameWithoutExtension(String fileName) {
		String[] fileNameAsArray = fileName.split("\\.");
		return fileNameAsArray[0];
	 }

}
