package com.capiot.ckyclistener.service.impl;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.capiot.ckyclistener.service.RequestFileReader;


@Component
public class RequestFileReaderImpl implements RequestFileReader{
	

	@Autowired
	Environment environment;
	
	private static final Logger logger = LoggerFactory.getLogger(RequestFileReaderImpl.class);
	private String home = System.getProperty("user.home");
	private String fileSeparator = System.getProperty("file.separator");
	
	private int batchNumber=0;
	
	
	/*Method to unzip the folder*/
	@Override
	public void unzipInputFolder(String fileName) {
		
		String pathToInputFolder = home+environment.getProperty("commonPathInput");
		
		File outerFolder = new File(pathToInputFolder+fileName);
		if(!outerFolder.exists()) {
			outerFolder.mkdirs();
		}
		
		byte[] buffer = new byte[1024];
		try(ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(pathToInputFolder+fileName+".zip"))) {
			ZipEntry zipEntry = zipInputStream.getNextEntry();
			while(zipEntry!=null) {
				String fileNameInsideZip = zipEntry.getName();
				File newFile = new File(pathToInputFolder+File.separator+fileNameInsideZip);
				logger.info("Unzipping to "+newFile.getAbsolutePath());
				new File(newFile.getParent()).mkdirs();
				try(FileOutputStream fileOutputStream = new FileOutputStream(newFile)){
					int length;
					while((length=zipInputStream.read(buffer))>0) {
						fileOutputStream.write(buffer, 0, length);
					}
				}
				zipEntry = zipInputStream.getNextEntry();
			}
		} catch (FileNotFoundException e) {
			logger.debug("Zip file not found");
			e.printStackTrace();
		} catch (IOException e) {
			logger.debug("Error in reading the zip file");
			e.printStackTrace();
		}
	}
	
	
	
	
	
	@Override
	public String getAllData(String fileName) throws IOException {
		
		String pathToInputFolder = home+environment.getProperty("commonPathInput");
		
		String inputFileAsString = "";
		try(BufferedReader bufferedReader = new BufferedReader(new FileReader(pathToInputFolder+fileName+fileSeparator+fileName+".txt"))) {
			String currentLine;
			while((currentLine = bufferedReader.readLine())!=null) {
				inputFileAsString=inputFileAsString.concat(currentLine).concat(System.getProperty("line.separator"));
			}
		}catch(FileNotFoundException e) {
			logger.debug("Input file is not found");
			e.printStackTrace();
		}
		return inputFileAsString;
	}
	
	
	
	@Override
	public String getBatchNumber(String allData) {
		batchNumber++;
		return new DecimalFormat("00").format(batchNumber);	
	}
	
	
	@Override
	public String getFIReferenceNumber(String allData) {
		String[] arrayOfLines = allData.split(System.getProperty("line.separator"));
		String secondLine = arrayOfLines[1];
		if(secondLine.startsWith("20")) {
			String[] arrayOfDetailRecords = secondLine.split(Pattern.quote("|"));
			return arrayOfDetailRecords[18];
		}else {
			return null;
		}
	}

	
	

	@Override
	public String getFICode(String allData) {
		String[] arraysOfLines = allData.split(System.getProperty("line.separator"));
		String firstLine = arraysOfLines[0];
		if(firstLine.startsWith("10")) {
			String[] arrayOfHeaders = firstLine.split(Pattern.quote("|"));
			return arrayOfHeaders[2];
		}else {
			return null;
		}
	}
	



	public int getNumberOfDetailRecords(String allData) {
		int counterForNumberOfDetailRecords = 0;
		String[] arraysOfLines = allData.split(System.getProperty("line.separator"));
		for(String singleLine:arraysOfLines) {
			if(singleLine.startsWith("20")) {
				counterForNumberOfDetailRecords++;
			}
		}
		return counterForNumberOfDetailRecords;
	}

}
