package com.capiot.ckyclistener.service.impl;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capiot.ckyclistener.service.RequestFileReader;
import com.capiot.ckyclistener.service.WriteDetailRecord;

@Component
public class WriteDetailRecordImpl implements WriteDetailRecord{
	
	private static final String INDIAN_TIMEZONE = "GMT+5:30";
	long ckycTempReferenceNumber = 10112919283L;
	long ckycNumber = 30012812731827L;
	
	
	@Autowired
	RequestFileReader requestFileReaderService;
	
	@Override
	public String writeRecordType() {
		return "20".concat("|");
	}

	@Override
	public String writeLineNumber(String singleDetailRecord) {
		String[] arrayOfDetailRecord = singleDetailRecord.split(Pattern.quote("|"));
		return arrayOfDetailRecord[1].concat("|");
	}

	@Override
	public String writeBatchName(String allData) {
		String[] arrayOfLines = allData.split(System.getProperty("line.separator"));
		String header = arrayOfLines[0];
		String[] arrayOfHeaders = header.split(Pattern.quote("|"));
		return arrayOfHeaders[1].concat("_").concat(getCurrentDateTime()).concat("|");
	}

	@Override
	public String writeCKYCTempReferenceNumber() {
		ckycTempReferenceNumber++;
		return String.valueOf(ckycTempReferenceNumber).concat("|");
	}

	@Override
	public String writeRequestType() {
		return "01".concat("|");
	}

	@Override
	public String writeResponseType() {
		return "05".concat("|");
	}

	@Override
	public String writeRecordStatus() {
		return "01".concat("|");	
	}

	@Override
	public String writeFIReferenceNumber(String singleDetailRecord) {
		String[] arrayOfDetailRecord = singleDetailRecord.split(Pattern.quote("|"));
		return arrayOfDetailRecord[18].concat("|");
	}

	@Override
	public String writeCKYCNumber() {
		ckycNumber++;
		return String.valueOf(ckycNumber).concat("|");
	}

	@Override
	public String writeRemarks() {
		return "|";
	}
	
	@Override
	public String writeNewLine() {
		return System.getProperty("line.separator");
	}
	
	
	/*Method to return current date time */
	public String getCurrentDateTime() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
		dateFormat.setTimeZone(TimeZone.getTimeZone(INDIAN_TIMEZONE));
		Date now = new Date();
		String currentDateTime = dateFormat.format(now);
		return currentDateTime;
	}

}
