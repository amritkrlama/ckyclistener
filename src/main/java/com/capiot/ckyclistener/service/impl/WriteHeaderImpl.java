package com.capiot.ckyclistener.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.springframework.stereotype.Component;

import com.capiot.ckyclistener.service.WriteHeader;

@Component
public class WriteHeaderImpl implements WriteHeader{
	
	private static final String INDIAN_TIMEZONE = "GMT+5:30";
	
	
	@Override
	public String writeRecordType() {
		return "10".concat("|");
	}

	@Override
	public String writeFICode(String FICode) {
		return FICode.concat("|");
	}

	@Override
	public String writeTotalNumberOfRecords(int totalNumberOfDetailRecords) {
		return String.valueOf(totalNumberOfDetailRecords).concat("|");
	}

	@Override
	public String writeVersionNumber() {
		return "V1.2".concat("|");
	}

	@Override
	public String writeCreateDate() {
		return getCurrentDateTime().concat("|");
	}

	@Override
	public String writeFillers() {
		return "|".concat("|").concat("|");
	}
	

	@Override
	public String writeNewLine() {
		return System.getProperty("line.separator");
	}
	
	/*Method to return current date time */
	public String getCurrentDateTime() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		dateFormat.setTimeZone(TimeZone.getTimeZone(INDIAN_TIMEZONE));
		Date now = new Date();
		String currentDateTime = dateFormat.format(now);
		return currentDateTime;
	}


}
