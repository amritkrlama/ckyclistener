package com.capiot.ckyclistener.service.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capiot.ckyclistener.service.CreateResponseFile;
import com.capiot.ckyclistener.service.WriteDetailRecord;
import com.capiot.ckyclistener.service.WriteHeader;
import com.capiot.ckyclistener.service.WriteResponseFile;

@Component
public class WriteResponseFileImpl implements WriteResponseFile {
	
	@Autowired
	CreateResponseFile createResponseFileService;
	@Autowired
	WriteHeader writeHeaderService;
	@Autowired
	WriteDetailRecord writeDetailRecordService;
	
	
	@Override
	public void writeResponseFile(String allData, String FICode, String batchNumber, String FIReferenceNumber, int numberOfDetailRecords, File file) {

		String result = "";
		
		result=result.concat(writeHeader(allData, FICode, batchNumber, FIReferenceNumber, numberOfDetailRecords));
		result=result.concat(writeDetailRecord(allData, FICode, batchNumber, FIReferenceNumber, numberOfDetailRecords));
		
		
		writeToFile(result, file);
	}

	@Override
	public String writeHeader(String allData, String FICode, String batchNumber, String FIReferenceNumber,  int numberOfDetailRecords) {
		String header = "";
		
		header=header.concat(writeHeaderService.writeRecordType());
		header=header.concat(writeHeaderService.writeFICode(FICode));
		header=header.concat(writeHeaderService.writeTotalNumberOfRecords(numberOfDetailRecords));
		header=header.concat(writeHeaderService.writeVersionNumber());
		header=header.concat(writeHeaderService.writeCreateDate());
		header=header.concat(writeHeaderService.writeFillers());
		header=header.concat(writeHeaderService.writeNewLine());
		
		return header;
	}

	@Override
	public String writeDetailRecord(String allData, String FICode, String batchNumber, String FIReferenceNumber,  int numberOfDetailRecords) {
		String detailRecord = "";
		
		String[] arrayOfLines = allData.split(System.getProperty("line.separator"));
		for(String singleLine: arrayOfLines) {
			if(singleLine.startsWith("20")) {
				detailRecord=detailRecord.concat(writeDetailRecordService.writeRecordType());
				detailRecord=detailRecord.concat(writeDetailRecordService.writeLineNumber(singleLine));
				detailRecord=detailRecord.concat(writeDetailRecordService.writeBatchName(allData));
				detailRecord=detailRecord.concat(writeDetailRecordService.writeCKYCTempReferenceNumber());
				detailRecord=detailRecord.concat(writeDetailRecordService.writeRequestType());
				detailRecord=detailRecord.concat(writeDetailRecordService.writeResponseType());
				detailRecord=detailRecord.concat(writeDetailRecordService.writeRecordStatus());
				detailRecord=detailRecord.concat(writeDetailRecordService.writeFIReferenceNumber(singleLine));
				detailRecord=detailRecord.concat(writeDetailRecordService.writeCKYCNumber());
				detailRecord=detailRecord.concat(writeDetailRecordService.writeRemarks());
				detailRecord=detailRecord.concat(writeDetailRecordService.writeNewLine());
			}
		}
	
		return detailRecord;
	}
	
	
	
	/* Method to write the result(String) to the file */
	public void writeToFile(String result, File file) {	
		try(BufferedWriter writer = new BufferedWriter(new FileWriter(file, true))){
			writer.write(result);
		}catch(IOException exception) {
			exception.printStackTrace();
		}
	}

}
